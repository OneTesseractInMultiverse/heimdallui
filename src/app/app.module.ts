import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {VexModule} from '../@vex/vex.module';
import {HttpClientModule} from '@angular/common/http';
import {CustomLayoutModule} from './custom-layout/custom-layout.module';
import {SecondaryToolbarModule} from '../@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from '../@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material';
import {IconModule} from '@visurel/iconify-angular';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,

        // Vex
        VexModule,
        CustomLayoutModule,
        SecondaryToolbarModule,
        BreadcrumbsModule,
        MatIconModule,
        IconModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
