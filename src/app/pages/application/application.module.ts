import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ApplicationRoutingModule} from './application-routing.module';
import {ListApplicationComponent} from './list-application/list-application.component';
import {CreateApplicationComponent} from './create-application/create-application.component';
import {UpdateApplicationComponent} from './update-application/update-application.component';
import {VexModule} from '../../../@vex/vex.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {SecondaryToolbarModule} from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material';
import {IconModule} from '@visurel/iconify-angular';


@NgModule({
    declarations: [
        ListApplicationComponent,
        CreateApplicationComponent,
        UpdateApplicationComponent
    ],
    imports: [
        CommonModule,
        VexModule,
        ApplicationRoutingModule,
        HttpClientModule,
        SecondaryToolbarModule,
        BreadcrumbsModule,
        MatIconModule,
        IconModule,
    ]
})
export class ApplicationModule {
}
