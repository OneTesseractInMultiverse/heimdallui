import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListApplicationComponent} from './list-application/list-application.component';
import {UpdateApplicationComponent} from './update-application/update-application.component';
import {CreateApplicationComponent} from './create-application/create-application.component';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: ListApplicationComponent
            },
            {
                path: 'update/:app_id',
                component: UpdateApplicationComponent
            },
            {
                path: 'create',
                component: CreateApplicationComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApplicationRoutingModule {
}
