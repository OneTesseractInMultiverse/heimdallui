import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CustomLayoutComponent} from './custom-layout/custom-layout.component';
import {CreateApplicationComponent} from './pages/application/create-application/create-application.component';
import {UpdateApplicationComponent} from './pages/application/update-application/update-application.component';
import {ListApplicationComponent} from './pages/application/list-application/list-application.component';
import {VexRoutes} from '../@vex/interfaces/vex-route.interface';


const applicationRoutes: VexRoutes = [
    {
        path: 'application',
        loadChildren: () => import('./pages/application/application.module').then(m => m.ApplicationModule)
    }
];

const routes: Routes = [
    {
        path: '',
        component: CustomLayoutComponent,
        children: applicationRoutes
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        // preloadingStrategy: PreloadAllModules,
        scrollPositionRestoration: 'enabled',
        relativeLinkResolution: 'corrected',
        anchorScrolling: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
